<?php
/**
 * Created by PhpStorm.
 * User: 86151 - 姜伟
 * Date: 2021/12/3 17:17
 */

namespace AnchuPac\PwdSlg\CheckPassword\Consts;


class CheckPasswordConst
{

    // 后台账号输入密码错误的redis的key
    const INPUT_PASSWORD_RECORD_KEY = 'adminer:inputpassword:errortimes:%s'; // %s=$adminId(管理员用户ID)

    // 锁定后台账号的redis的key
    const LOCK_ACCOUNT_BY_USERNAME_KEY = 'adminer:lock:username:%s'; // %s=$username(管理员用户名)

    // 后台账号输入密码错误的次数
    const INPUT_PASSWORD_ERROR_TIMES = 5;

    // 后台账号输入密码错误达到峰值时，锁定账号的时间
    const INPUT_PASSWORD_ERROR_LOCK_EXPIRE_TIME = 300; // 暂定5分钟

    // 后台用户操作之后存进redis的key
    const LONGTIME_ACTION_KEY = 'adminer:longtime:action:%s'; // %s=$adminId(管理员用户id)

    // 后台用户长期操作的超时时间
    const ADMINER_LONGTIME_ACTION_EXPIRE_TIME = 1800; // 默认30分钟

    // 账号登录类型
    const LOGIN_TYPE = 1; // 用户名密码登录

}