<?php
/**
 * Created by PhpStorm.
 * User: 86151 - 姜伟
 * Date: 2021/12/3 17:49
 */

namespace AnchuPac\PwdSlg\Common\Util;


class ResUtil
{

    use SingletonUtil;

    // 成功返回
    public function responseSuccess($data = [], $msg = '成功')
    {
        return $this->response(1, $msg, $data);
    }

    // 失败返回
    public function responseFail($msg = '失败', $data = [])
    {
        return $this->response(-1, $msg, $data);
    }

    // 返回
    private function response($code = 1, $msg = '', $data = [])
    {
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }

}