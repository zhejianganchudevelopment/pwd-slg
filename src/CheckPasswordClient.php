<?php
/**
 * Created by PhpStorm.
 * User: 86151 - 姜伟
 * Date: 2021/12/3 17:15
 */

namespace AnchuPac\PwdSlg;

use AnchuPac\PwdSlg\CheckPassword\Libs\CheckPasswordSlg;
use AnchuPac\PwdSlg\Common\Util\SingletonUtil;

class CheckPasswordClient
{

    use SingletonUtil;

    // 检测X分钟内连续输入密码错误次数是否达到峰值
    public function checkInputPasswordTimes($adminId = '', $username = '')
    {
        return CheckPasswordSlg::getInstance()->checkInputPasswordTimes($adminId, $username);
    }

}