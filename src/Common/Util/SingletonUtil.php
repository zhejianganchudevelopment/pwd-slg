<?php
/**
 * Created by PhpStorm.
 * User: 86151 - 姜伟
 * Date: 2021/5/10
 * Time: 17:35
 */

namespace AnchuPac\PwdSlg\Common\Util;

trait SingletonUtil
{
    private static $instance;

    static function getInstance(...$args)
    {
        if(!isset(self::$instance)){
            self::$instance = new static(...$args);
        }
        return self::$instance;
    }

}
